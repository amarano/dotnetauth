﻿using System;
using DotNetAuth.OAuth2.Framework;

namespace DotNetAuth.OAuth2
{
    /// <summary>
    /// A basic structure for collecting output parameters of user's response to authorization request.
    /// </summary>
    /// <remarks>
    /// After user decides on whether to  <see cref="AllParameters"/>
    /// </remarks>
    public class ProcessUserResponseOutput
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessUserResponseOutput"/> class with access token arguments.
        /// </summary>
        /// <remarks>
        /// <para>You usually don't need to construct an instance of this class, unless you are implementing a OAuth provider.</para>
        /// </remarks>
        /// <param name="allParameters">All parameters.</param>
        /// <param name="accessToken">The access token.</param>
        /// <param name="expires">The time in which expires access token expires.</param>
        /// <param name="refreshToken">The refresh token to request for a new access token.</param>
        public ProcessUserResponseOutput(ParameterSet allParameters, string accessToken, DateTime? expires = null, string refreshToken = null)
        {
            Succeed = true;
            Failed = false;
            this.AllParameters = allParameters;
            this.AccessToken = accessToken;
            this.Expires = expires;
            this.RefreshToken = refreshToken;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessUserResponseOutput"/> class with error arguments.
        /// </summary>
        /// <param name="allParameters">All parameters.</param>
        /// <param name="error">The error.</param>
        /// <param name="reason">The reason.</param>
        /// <param name="description">The description.</param>
        public ProcessUserResponseOutput(ParameterSet allParameters, string error, string reason, string description)
        {
            Succeed = false;
            Failed = true;
            AllParameters = allParameters;
            Error = new ErrorInfo {
                Denied = error == "access_denied",
                UserDenied = reason == "user_denied",
                Code = error,
                Reason = reason,
                Description = description,
            };
        }

        /// <summary>
        /// If user granted the access and the access token is issued, then returns true.
        /// </summary>
        public bool Succeed { get; private set; }
        /// <summary>
        /// If succeeded then gets the access token.
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// If succeeded then gets then refresh token. Not all providers give out a refresh token, or sometimes you need to explicitly ask for a refresh token by specifying a special value for scope parameter.
        /// </summary>
        public string RefreshToken { get; set; }
        /// <summary>
        /// Gets a time for when the access token will expire.
        /// </summary>
        /// <remarks>
        /// <para>Not all providers give out this value. Sometimes the access token does not get expired.</para>
        /// <para>You may use this value in combination with access token to update your access token if it is expired.</para>        
        /// </remarks>
        public DateTime? Expires { get; set; }
        /// <summary>
        /// Gets the state value you passed to authorization endpoint.
        /// </summary>
        /// <remarks>
        /// State value is an arbitrary value that you could pass to OAuth provider when redirecting your site's user to their authorization endpoint. And afterward when
        /// users are returned back to your website you receive the same value. You may use this value for any purpose.
        /// </remarks>
        public string State { get; set; }
        /// <summary>
        /// If user rejected your request for access, or some thing unexpected happens, then returns true.
        /// </summary>        
        public bool Failed { get; private set; }
        /// <summary>
        /// Gets an object with detailed information about what kind of error happened.
        /// </summary>
        public ErrorInfo Error { get; private set; }
        /// <summary>
        /// Gets the whole set of parameters in the response.
        /// </summary>
        /// <remarks>
        /// This property holds all the parameters in output. Only some of those parameters is accessible by properties of <see cref="ProcessUserResponseOutput"/>, so this propery gives complete
        /// access to the list of all parameters provided in output.
        /// </remarks>
        public ParameterSet AllParameters { get; set; }

        /// <summary>
        /// Encapsulates error information of a <see cref="ProcessUserResponseOutput"/>.
        /// </summary>
        public class ErrorInfo
        {
            /// <summary>
            /// If true means user or service provider denied to authorize you.
            /// </summary>
            public bool Denied { get; set; }
            /// <summary>
            /// If true means user denied to authorize you.
            /// </summary>
            public bool UserDenied { get; set; }
            /// <summary>
            /// The error code provided by service provider.
            /// </summary>
            /// <remarks>
            /// This property can have different values based on the service provider you are trying to communicate. You can use this code for diagnostic purposes.            
            /// redirect_uri_mismatch : means that you have specified a wrong redirect uri during registering your application
            /// or you have passed a wrong value to <see cref="OAuth2Process.GetAuthenticationUri"/>.
            /// </remarks>
            public string Code { get; set; }
            /// <summary>
            /// Gives the reason why your request for authorization failed, if any reason is provided.
            /// </summary>
            public string Reason { get; set; }
            /// <summary>
            /// Gets a description of error.
            /// </summary>
            public string Description { get; set; }
        }
    }
}
