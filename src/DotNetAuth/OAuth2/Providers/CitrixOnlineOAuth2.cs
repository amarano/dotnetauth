﻿namespace DotNetAuth.OAuth2.Providers
{
    /// <summary>
    /// The OAuth2 provider for Citrix Online.
    /// </summary>
    public class CitrixOnlineOAuth2 : OAuth2ProviderDefinition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CitrixOnlineOAuth2"/> class.
        /// </summary>
        public CitrixOnlineOAuth2()
        {
            AuthorizationEndpointUri = "https://api.citrixonline.com/oauth/authorize";
            TokenEndpointUri = "https://api.citrixonline.com/oauth/access_token";
        }
    }
}
