﻿using System;
using System.Threading.Tasks;
using DotNetAuth.OAuth1a;
using DotNetAuth.OAuth2;

namespace DotNetAuth.Profiles
{
    public class Login
    {
        public static Task<Uri> GetAuthenticationUri(LoginProvider provider, Uri loginProcessUri, ILoginStateManager stateManager, ProfileProperty[] requiredProperties)
        {
            var generalLoginStataManager = new LoginStateManager(stateManager);
            switch (provider.Definition.ProtocolType) {
            case ProtocolTypes.OAuth1a:
                return OAuth1aProcess.GetAuthorizationUri(provider.Definition.GetOAuth1aDefinition(), provider.GetOAuth1aCredentials(), loginProcessUri.AbsoluteUri, generalLoginStataManager);
            case ProtocolTypes.OAuth2:
                var scope = provider.Definition.GetRequiredScope(requiredProperties);
                return Task.Factory.StartNew<Uri>(() => OAuth2Process.GetAuthenticationUri(provider.Definition.GetOAuth2Definition(), provider.GetOAuth2Credentials(), loginProcessUri.AbsoluteUri, scope, generalLoginStataManager));
            default:
                throw new Exception("Invalid provider. Provider's protocol type is not set or supported.");
            }
        }
        public static Task<Profile> GetProfile(LoginProvider provider, Uri requestedUri, string redirectUri, ILoginStateManager stateManager, ProfileProperty[] requiredProperties)
        {
            var generalLoginStataManager = new LoginStateManager(stateManager);
            switch (provider.Definition.ProtocolType) {
            case ProtocolTypes.OAuth1a:
                var token_secret = stateManager.LoadTemp();
                return
                    OAuth1aProcess.ProcessUserResponse(provider.Definition.GetOAuth1aDefinition(), provider.GetOAuth1aCredentials(), requestedUri, generalLoginStataManager)
                    .ContinueWith<Profile>(response => {
                        var oauth_token = response.Result.AllParameters["oauth_token"];
                        var oauth_token_secret = response.Result.AllParameters["oauth_token_secret"];
                        var http = new RestSharp.Http { Url = new Uri(provider.Definition.GetProfileEndpoint(requiredProperties)) };
                        OAuth10aAuthorizedCalls.ApplyAccessTokenToHeader(http, provider.Definition.GetOAuth1aDefinition(), provider.GetOAuth1aCredentials(), oauth_token, oauth_token_secret, "GET");
                        var profileContent = http.Get();
                        var profile = provider.Definition.ParseProfile(profileContent.Content);
                        stateManager.ClearTemp();
                        return profile;
                    });
            case ProtocolTypes.OAuth2:
                return
                    OAuth2Process.ProcessUserResponse(provider.Definition.GetOAuth2Definition(), provider.GetOAuth2Credentials(), requestedUri, redirectUri, generalLoginStataManager)
                    .ContinueWith(response => {
                        stateManager.ClearTemp();
                        var access_token = response.Result.AccessToken;
                        var http = new RestSharp.Http();
                        http.Url = OAuthAuthorizedCalls.AddAccessTokenToQueryString(new Uri(provider.Definition.GetProfileEndpoint(requiredProperties)), access_token);
                        //oauth2.OAuthAuthorizedCalls.ApplyAccessTokenToHeader(http, access_token);
                        var profileResponse = http.Get();
                        var profileContent = profileResponse.Content;
                        return provider.Definition.ParseProfile(profileContent);
                    });
            default:
                throw new Exception("Invalid provider. Provider's protocol type is not set or supported.");

            }
        }
    }
}
