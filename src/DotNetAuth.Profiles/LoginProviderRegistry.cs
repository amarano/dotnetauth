using System.Collections.Generic;
using System.Linq;

namespace DotNetAuth.Profiles
{
    public class LoginProviderRegistry
    {
        static LoginProviderRegistry()
        {
            Facebook = new FacebookLoginProviderDefinition();
            Google = new GoogleLoginProviderDefinition();
            Twitter = new TwitterLoginProviderDefinition();
            Yahoo = new YahooLoginProviderDefinition();
            Microsoft = new MicrosoftLoginProviderDefinition();
            providers = new List<LoginProviderDefinition>(new[] { Facebook, Google, Microsoft, Yahoo, Twitter });
        }
        private static List<LoginProviderDefinition> providers;
        public static LoginProviderDefinition Facebook { get; set; }
        public static LoginProviderDefinition Google { get; set; }
        public static LoginProviderDefinition Twitter { get; set; }
        public static LoginProviderDefinition Microsoft { get; set; }
        public static LoginProviderDefinition Yahoo { get; set; }
        public static LoginProviderDefinition LinkedIn { get; set; }
        public static void Register(LoginProviderDefinition provider)
        {
            var oldVersion = Get(provider.Name);
            providers.Add(provider);
            providers.Remove(oldVersion);
        }
        public static LoginProviderDefinition Get(string providerName)
        {
            return providers.Where(p => string.Compare(p.Name, providerName, true) == 0).FirstOrDefault();
        }
    }
}
