namespace DotNetAuth.Profiles
{
    public enum ProtocolTypes
    {
        OAuth1a,
        OAuth2
    }
}
