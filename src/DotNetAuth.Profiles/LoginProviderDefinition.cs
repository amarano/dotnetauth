using System;

namespace DotNetAuth.Profiles
{
    public abstract class LoginProviderDefinition
    {
        public LoginProviderDefinition()
        {
        }
        public LoginProviderDefinition(string name, string fullName, Type definitionType, ProtocolTypes type)
        {
            this.Name = name;
            this.Fullname = fullName;
            this.ProtocolType = type;
        }
        public string Name { get; set; }
        public string Fullname { get; set; }
        public ProtocolTypes ProtocolType { get; set; }
        public abstract ProfileProperty[] GetSupportedProperties();
        public abstract Profile ParseProfile(string content);
        public abstract string GetProfileEndpoint(ProfileProperty[] requiredProperties);
        public abstract string GetRequiredScope(ProfileProperty[] requiredProperties);
        public virtual DotNetAuth.OAuth1a.OAuth1aProviderDefinition GetOAuth1aDefinition()
        {
            return null;
        }
        public virtual DotNetAuth.OAuth2.OAuth2ProviderDefinition GetOAuth2Definition()
        {
            return null;
        }
    }
}
