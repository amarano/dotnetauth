namespace DotNetAuth.Profiles
{
    public class GoogleLoginProviderDefinition : LoginProviderDefinition
    {
        ProfileProperty[] supportedProperties;
        public GoogleLoginProviderDefinition()
        {
            Name = "Google";
            Fullname = "Google";
            ProtocolType = ProtocolTypes.OAuth2;
            var userProfileScope = "https://www.googleapis.com/auth/userinfo.profile";
            var emailScope = "https://www.googleapis.com/auth/userinfo.email";
            supportedProperties = new[] {
                ProfileProperty.UniqueID        .Result("id")           .Scope(userProfileScope),
                ProfileProperty.DisplayName     .Result("name")         .Scope(userProfileScope),
                ProfileProperty.FirstName       .Result("given_name")   .Scope(userProfileScope),
                ProfileProperty.LastName        .Result("family_name")  .Scope(userProfileScope),
                ProfileProperty.ProfileLink     .Result("link")         .Scope(userProfileScope),
                ProfileProperty.FullName        .Result("name")         .Scope(userProfileScope),
                ProfileProperty.PictureLink     .Result("picture")      .Scope(userProfileScope),
                ProfileProperty.Gender          .Result("gender")       .Scope(userProfileScope),
                ProfileProperty.BirthDate       .Result("birthday")     .Scope(userProfileScope),
                ProfileProperty.Timezone        .Result("timezone")     .Scope(userProfileScope),
                ProfileProperty.Email           .Result("email")        .Scope(emailScope),
            };
        }
        public override ProfileProperty[] GetSupportedProperties()
        {
            return supportedProperties;
        }
        public override string GetProfileEndpoint(ProfileProperty[] requiredProperties)
        {
            return "https://www.googleapis.com/oauth2/v2/userinfo";
        }
        public override string GetRequiredScope(ProfileProperty[] requiredProperties)
        {
            return supportedProperties.GetScope(requiredProperties, " ");
        }
        public override Profile ParseProfile(string content)
        {
            var json = Newtonsoft.Json.Linq.JObject.Parse(content);
            var result = new Profile {
                UniqueID = json.Value<string>("id"),
                DisplayName = json.Value<string>("name"),
                FirstName = json.Value<string>("given_name"),
                LastName = json.Value<string>("family_name"),
                ProfileLink = json.Value<string>("link"),
                FullName = json.Value<string>("name"),
                PictureLink = json.Value<string>("picture"),
                Gender = json.Value<string>("gender"),
                BirthDate = json.Value<string>("birthday"),
                Timezone = json.Value<string>("timezone"),
                Email = json.Value<string>("email")
            };
            return result;
        }
        public override DotNetAuth.OAuth2.OAuth2ProviderDefinition GetOAuth2Definition()
        {
            return new DotNetAuth.OAuth2.Providers.GoogleOAuth2();
        }
    }
}
