using Newtonsoft.Json.Linq;
namespace DotNetAuth.Profiles
{
    public class MicrosoftLoginProviderDefinition : LoginProviderDefinition
    {
        ProfileProperty[] supportedProperties;

        public MicrosoftLoginProviderDefinition()
        {
            Name = "Microsoft";
            Fullname = "Microsoft Account";
            ProtocolType = ProtocolTypes.OAuth2;
        }
        public override ProfileProperty[] GetSupportedProperties()
        {
            supportedProperties = new[] {
             ProfileProperty.UniqueID,
             ProfileProperty.DisplayName,
             ProfileProperty.FirstName,
             ProfileProperty.LastName,
             ProfileProperty.ProfileLink,
             ProfileProperty.BirthDate,
             ProfileProperty.Gender,
             ProfileProperty.Email,
             ProfileProperty.Locale,             
            };
            return supportedProperties;
        }
        public override string GetProfileEndpoint(ProfileProperty[] requiredProperties)
        {
            return "https://apis.live.net/v5.0/me/";
        }
        public override string GetRequiredScope(ProfileProperty[] requiredProperties)
        {
            //http://msdn.microsoft.com/en-us/library/live/hh243648.aspx#user
            return "wl.basic,wl.birthday,wl.emails,wl.postal_addresses";
        }
        public override Profile ParseProfile(string content)
        {
            var json = JObject.Parse(content);
            var result = new Profile();
            result.UniqueID = json.Value<string>("id");
            result.FullName = json.Value<string>("name");
            result.FirstName = json.Value<string>("first_name");
            result.LastName = json.Value<string>("last_name");
            result.ProfileLink = json.Value<string>("link");
            var year = json.Value<string>("birth_year");
            var month = json.Value<string>("birth_month");
            var day = json.Value<string>("birth_day");
            if (year != null && month != null && day != null) {
                result.BirthDate = year + "/" + month + "/" + day;
            }
            result.Gender = json.Value<string>("gender");
            result.Email = json.Value<JObject>("emails").Value<string>("account");
            result.Locale = json.Value<string>("locale");
            return result;
        }
        public override DotNetAuth.OAuth2.OAuth2ProviderDefinition GetOAuth2Definition()
        {
            return new DotNetAuth.OAuth2.Providers.LiveOAuth2();
        }
    }
}
