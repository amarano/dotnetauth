using System;
using System.Web;

namespace DotNetAuth.Profiles
{
    public interface ILoginStateManager
    {
        void SaveTemp(string stateCode);
        string LoadTemp();
        void ClearTemp();
    }

    public class DefaultLoginStateManager : ILoginStateManager
    {
        HttpSessionStateBase session;
        public DefaultLoginStateManager(HttpSessionStateBase session)
        {
            this.session = session;
        }
        public void SaveTemp(string stateCode)
        {
            session["login_temp_state"] = stateCode;
        }

        public string LoadTemp()
        {
            return session["login_temp_state"] as string;
        }

        public void ClearTemp()
        {
            session.Remove("login_temp_state");
        }
    }

    public class LoginStateManager : DotNetAuth.OAuth1a.IOAuth10aStateManager, DotNetAuth.OAuth2.IOAuth20StateManager
    {
        ILoginStateManager stateManager;

        public LoginStateManager(ILoginStateManager stateManager)
        {
            this.stateManager = stateManager;
        }

        #region IOAuth10aStateManager
        public void SaveTemporaryTokenSecret(string requestToken, string oauth_token_secret)
        {
            stateManager.SaveTemp(oauth_token_secret);
        }
        public string LoadTemporaryTokenSecret(string requestToken)
        {
            return stateManager.LoadTemp();
        }
        #endregion

        #region IOauth20StateManager
        public string GetState()
        {
            var value = Guid.NewGuid().ToString("D");
            stateManager.SaveTemp(value);
            return value;
        }
        public bool CheckState(string stateCode)
        {
            var savedValue = stateManager.LoadTemp();
            if (stateCode == savedValue)
                return true;
            else
                return false;
        }
        #endregion
    }
}
