﻿using System.Collections.Generic;
using System.Linq;

namespace DotNetAuth.Profiles
{
    public class LoginProvider
    {
        #region properties
        public LoginProviderDefinition Definition { get; set; }
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        #endregion
        #region methods
        public DotNetAuth.OAuth1a.ApplicationCredentials GetOAuth1aCredentials()
        {
            return new DotNetAuth.OAuth1a.ApplicationCredentials { ConsumerKey = AppId, ConsumerSecret = AppSecret };
        }
        public DotNetAuth.OAuth2.ApplicationCredentials GetOAuth2Credentials()
        {
            return new DotNetAuth.OAuth2.ApplicationCredentials { AppId = AppId, AppSecretId = AppSecret };
        }
        #endregion

        #region static constructor
        static LoginProvider()
        {
            providers = new List<LoginProvider>();
        }
        #endregion
        #region static fields
        private static List<LoginProvider> providers;
        #endregion
        #region static methods
        public static void SetKeys(LoginProviderDefinition provider, string appId, string appSecret)
        {
            SetKeys(provider.Name, appId, appSecret);
        }
        public static void SetKeys(string providerName, string appId, string appSecret)
        {
            var provider = Get(providerName);
            if (provider == null) {
                var providerDefinition = LoginProviderRegistry.Get(providerName);
                providers.Add(new LoginProvider { Definition = providerDefinition, AppId = appId, AppSecret = appSecret });
            }
            else {
                provider.AppId = appId;
                provider.AppSecret = appSecret;
            }
        }
        public static LoginProvider Get(string providerName)
        {
            return providers.Where(p => string.Compare(p.Definition.Name, providerName, true) == 0).FirstOrDefault();
        }
        #endregion
    }
}
