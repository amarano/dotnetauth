using DotNetAuth.OAuth1a.Framework;
using NUnit.Framework;

namespace DotNetAuth.OAuth1.Framework.Tests
{
    public class PercentEncodeTests
    {
        [TestFixture]
        public class EncodeMethod
        {
            [Test]
            public void IfAllInputCharactersAreAllowed_ShouldReturnSameString()
            {
                var text = "text";
                var expected = text;
                var result = PercentEncode.Encode(text);
                Assert.AreEqual(expected, result);
            }
            [Test]
            public void ShouldReplaceUnicodeCharactersInInput()
            {
                var text = "متن";
                var expected = "%D9%85%D8%AA%D9%86";
                var result = PercentEncode.Encode(text);
                Assert.AreEqual(expected, result);
            }
            [Test]
            public void ShouldReplaceNotAllowedSymbols()
            {
                var text = @"^@()`""\/,*+";
                var expected = "%5E%40%28%29%60%22%5C%2F%2C%2A%2B";
                var result = PercentEncode.Encode(text);
                Assert.AreEqual(expected, result);
            }
        }

        [TestFixture]
        public class DecodeMethod
        {
            [Test]
            public void IfAllInputCharactersAreAllowed_ShouldReturnSameString()
            {
                var text = "text";
                var expected = text;
                var result = PercentEncode.Decode(text);
                Assert.AreEqual(expected, result);
            }
            [Test]
            public void EscapedCharactersShouldBeRecovered()
            {
                var text = "%5E%40";
                var expected = "^@";
                var result = PercentEncode.Decode(text);
                Assert.AreEqual(expected, result);
            }
        }
    }
}
