using System.Web;

namespace oauth.samples.Controllers
{
    public class OAuth10aSessionStateManager : DotNetAuth.OAuth1a.IOAuth10aStateManager
    {
        private string key;
        private HttpSessionStateBase session;
        public OAuth10aSessionStateManager(HttpSessionStateBase session, string key)
        {
            this.session = session;
            this.key = key;
        }
        public void SaveTemporaryTokenSecret(string requestToken, string oauth_token_secret)
        {
            session[key] = oauth_token_secret;
        }
        public string LoadTemporaryTokenSecret(string requestToken)
        {
            return session[key] as string;
        }
    }
}
