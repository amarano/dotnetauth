﻿using System;
using System.Web.Mvc;

namespace oauth.samples.Controllers
{
    public partial class AuthController : Controller
    {
        public AuthController()
        {
            SetKeys();
        }
        public DotNetAuth.OAuth2.ApplicationCredentials FacebookAppCredentials;
        public DotNetAuth.OAuth2.ApplicationCredentials GoogleAppCredentials;
        public DotNetAuth.OAuth2.ApplicationCredentials LiveAppCredentials;
        public DotNetAuth.OAuth2.ApplicationCredentials MeetupAppCredentials;
        public DotNetAuth.OAuth2.ApplicationCredentials FoursquareAppCredentials;
        public DotNetAuth.OAuth2.ApplicationCredentials GithubAppCredentials;
        public DotNetAuth.OAuth2.ApplicationCredentials CitrixonlineAppCredentials;

        public DotNetAuth.OAuth1a.ApplicationCredentials TwitterAppCredentials;
        public DotNetAuth.OAuth1a.ApplicationCredentials LinkedInAppCredentials;
        public DotNetAuth.OAuth1a.ApplicationCredentials YahooAppCredentials;
        public DotNetAuth.OAuth1a.ApplicationCredentials FlickrAppCredentials;
        public DotNetAuth.OAuth1a.ApplicationCredentials VimeoAppCredentials;

        partial void SetKeys();


        OAuth2SessionStateManager oauth2StateManager;
        OAuth10aSessionStateManager oauth10aStateManager;

        protected override System.IAsyncResult BeginExecute(System.Web.Routing.RequestContext requestContext, System.AsyncCallback callback, object state)
        {
            oauth10aStateManager = new OAuth10aSessionStateManager(requestContext.HttpContext.Session, "oauth1_secret_token");
            oauth2StateManager = new OAuth2SessionStateManager(requestContext.HttpContext.Session);
            return base.BeginExecute(requestContext, callback, state);
        }

        public ActionResult Facebook()
        {
            var userProcessUri = Url.Action("FacebookResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var redirectUri = DotNetAuth.OAuth2.OAuth2Process.GetAuthenticationUri(new DotNetAuth.OAuth2.Providers.FacebookOAuth2(), FacebookAppCredentials, userProcessUri, "", oauth2StateManager);
            return Redirect(redirectUri.ToString());
        }
        public ActionResult FacebookResponseProcess()
        {
            var userProcessUri = Url.Action("FacebookResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var response = DotNetAuth.OAuth2.OAuth2Process.ProcessUserResponse(new DotNetAuth.OAuth2.Providers.FacebookOAuth2(), FacebookAppCredentials, Request.Url, userProcessUri, oauth2StateManager);
            response.Wait();
            Session["facebookaccesstoken"] = response.Result.AccessToken;
            Session["facebookexpire"] = response.Result.Expires;
            return View("AccessTokens");
        }

        public RedirectResult Google()
        {
            var userProcessUri = Url.Action("GoogleResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var scope = "https://www.googleapis.com/auth/userinfo.profile";
            var redirectUri = DotNetAuth.OAuth2.OAuth2Process.GetAuthenticationUri(new DotNetAuth.OAuth2.Providers.GoogleOAuth2(), GoogleAppCredentials, userProcessUri, scope, oauth2StateManager);
            return Redirect(redirectUri.ToString());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Google requires the address for this action to be exactly the same as the registered redirected URI in the Apps Console.
        /// </remarks>
        /// <returns></returns>
        public ActionResult GoogleResponseProcess()
        {
            var userProcessUri = Url.Action("GoogleResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var response = DotNetAuth.OAuth2.OAuth2Process.ProcessUserResponse(new DotNetAuth.OAuth2.Providers.GoogleOAuth2(), GoogleAppCredentials, Request.Url, userProcessUri, oauth2StateManager);
            response.Wait();
            Session["googleaccesstoken"] = response.Result.AccessToken;
            Session["googleexpire"] = response.Result.Expires;
            return View("AccessTokens");
        }

        public RedirectResult Live()
        {
            var userProcessUri = Url.Action("LiveResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var scope = "wl.basic wl.signin";
            var redirectUri = DotNetAuth.OAuth2.OAuth2Process.GetAuthenticationUri(new DotNetAuth.OAuth2.Providers.LiveOAuth2(), LiveAppCredentials, userProcessUri, scope, oauth2StateManager);
            return Redirect(redirectUri.ToString());
        }
        public ActionResult LiveResponseProcess()
        {
            var userProcessUri = Url.Action("LiveResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var response = DotNetAuth.OAuth2.OAuth2Process.ProcessUserResponse(new DotNetAuth.OAuth2.Providers.LiveOAuth2(), LiveAppCredentials, Request.Url, userProcessUri, oauth2StateManager);
            response.Wait();
            Session["liveaccesstoken"] = response.Result.AccessToken;
            Session["liveexpire"] = response.Result.Expires;
            return View("AccessTokens");
        }

        public RedirectResult Meetup()
        {
            var userProcessUri = Url.Action("MeetupResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var redirectUri = DotNetAuth.OAuth2.OAuth2Process.GetAuthenticationUri(new DotNetAuth.OAuth2.Providers.MeetupOAuth2(), MeetupAppCredentials, userProcessUri, "basic", oauth2StateManager);
            return Redirect(redirectUri.AbsoluteUri);
        }
        public ActionResult MeetupResponseProcess()
        {
            var userProcessUri = Url.Action("MeetupResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var response = DotNetAuth.OAuth2.OAuth2Process.ProcessUserResponse(new DotNetAuth.OAuth2.Providers.MeetupOAuth2(), MeetupAppCredentials, Request.Url, userProcessUri, oauth2StateManager);
            response.Wait();
            Session["meetupaccesstoken"] = response.Result.AccessToken;
            Session["meetupexpire"] = response.Result.Expires;
            return View("AccessTokens");
        }

        public RedirectResult Foursquare()
        {
            var userProcessUri = Url.Action("FoursquareResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var redirectUri = DotNetAuth.OAuth2.OAuth2Process.GetAuthenticationUri(new DotNetAuth.OAuth2.Providers.FoursquareOAuth2(), FoursquareAppCredentials, userProcessUri, "", oauth2StateManager);
            return Redirect(redirectUri.AbsoluteUri);
        }
        public ActionResult FoursquareResponseProcess()
        {
            var userProcessUri = Url.Action("FoursquareResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var response = DotNetAuth.OAuth2.OAuth2Process.ProcessUserResponse(new DotNetAuth.OAuth2.Providers.FoursquareOAuth2(), FoursquareAppCredentials, Request.Url, userProcessUri, oauth2StateManager);
            response.Wait();
            Session["foursquareaccesstoken"] = response.Result.AccessToken;
            Session["foursquareexpire"] = response.Result.Expires;
            return View("AccessTokens");
        }

        public RedirectResult Github()
        {
            var userProcessUri = Url.Action("GithubResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var redirectUri = DotNetAuth.OAuth2.OAuth2Process.GetAuthenticationUri(new DotNetAuth.OAuth2.Providers.GithubOAuth2(), GithubAppCredentials, userProcessUri, "", oauth2StateManager);
            return Redirect(redirectUri.AbsoluteUri);
        }
        public ActionResult GithubResponseProcess()
        {
            var userProcessUri = Url.Action("GithubResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var response = DotNetAuth.OAuth2.OAuth2Process.ProcessUserResponse(new DotNetAuth.OAuth2.Providers.GithubOAuth2(), GithubAppCredentials, Request.Url, userProcessUri, oauth2StateManager);
            response.Wait(TimeSpan.FromSeconds(30));
            Session["githubaccesstoken"] = response.Result.AccessToken;
            Session["githubexpire"] = response.Result.Expires;
            return View("AccessTokens");
        }

        public RedirectResult Citrixonline()
        {
            var userProcessUri = Url.Action("CitrixonlineResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var redirectUri = DotNetAuth.OAuth2.OAuth2Process.GetAuthenticationUri(new DotNetAuth.OAuth2.Providers.CitrixOnlineOAuth2(), CitrixonlineAppCredentials, userProcessUri, "", oauth2StateManager);
            return Redirect(redirectUri.AbsoluteUri);
        }
        public ActionResult CitrixonlineResponseProcess()
        {
            var userProcessUri = Url.Action("CitrixonlineResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var response = DotNetAuth.OAuth2.OAuth2Process.ProcessUserResponse(new DotNetAuth.OAuth2.Providers.CitrixOnlineOAuth2(), CitrixonlineAppCredentials, Request.Url, userProcessUri, oauth2StateManager);
            response.Wait();
            Session["citrixonlineaccesstoken"] = response.Result.AccessToken;
            Session["citrixonlineexpire"] = response.Result.Expires;
            return View("AccessTokens");
        }

        public RedirectResult Twitter()
        {
            var callback = Url.Action("TwitterResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var authorizationUri = DotNetAuth.OAuth1a.OAuth1aProcess.GetAuthorizationUri(new DotNetAuth.OAuth1a.Providers.TwitterOAuth1a(), TwitterAppCredentials, callback, oauth10aStateManager);
            authorizationUri.Wait();
            return Redirect(authorizationUri.Result.AbsoluteUri);
        }
        public ActionResult TwitterResponseProcess()
        {
            var response = DotNetAuth.OAuth1a.OAuth1aProcess.ProcessUserResponse(new DotNetAuth.OAuth1a.Providers.TwitterOAuth1a(), TwitterAppCredentials, Request.Url, oauth10aStateManager);
            response.Wait();
            Session["twitteraccesstoken"] = response.Result.AllParameters[DotNetAuth.OAuth1a.Framework.Names.AccessTokenResponse.oauth_token];
            return View("AccessTokens");
        }


        public RedirectResult LinkedIn()
        {
            var callback = Url.Action("LinkedInResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var authorizationUri = DotNetAuth.OAuth1a.OAuth1aProcess.GetAuthorizationUri(new DotNetAuth.OAuth1a.Providers.LinkedInOAuth1a(), LinkedInAppCredentials, callback, oauth10aStateManager);
            authorizationUri.Wait();
            return Redirect(authorizationUri.Result.AbsoluteUri);
        }
        public ActionResult LinkedInResponseProcess()
        {
            var response = DotNetAuth.OAuth1a.OAuth1aProcess.ProcessUserResponse(new DotNetAuth.OAuth1a.Providers.LinkedInOAuth1a(), LinkedInAppCredentials, Request.Url, oauth10aStateManager);
            response.Wait();
            Session["linkedinaccesstoken"] = response.Result.AllParameters[DotNetAuth.OAuth1a.Framework.Names.AccessTokenResponse.oauth_token];
            return View("AccessTokens");
        }


        public RedirectResult Yahoo()
        {
            var callback = Url.Action("YahooResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var authorizationUri = DotNetAuth.OAuth1a.OAuth1aProcess.GetAuthorizationUri(new DotNetAuth.OAuth1a.Providers.YahooOAuth1a(), YahooAppCredentials, callback, oauth10aStateManager);
            authorizationUri.Wait();
            return Redirect(authorizationUri.Result.AbsoluteUri);
        }
        public ActionResult YahooResponseProcess()
        {
            var response = DotNetAuth.OAuth1a.OAuth1aProcess.ProcessUserResponse(new DotNetAuth.OAuth1a.Providers.YahooOAuth1a(), YahooAppCredentials, Request.Url, oauth10aStateManager);
            response.Wait();
            Session["yahooaccesstoken"] = response.Result.AllParameters[DotNetAuth.OAuth1a.Framework.Names.AccessTokenResponse.oauth_token];
            return View("AccessTokens");
        }

        public RedirectResult Flickr()
        {
            var callback = Url.Action("FlickrResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var authorizationUri = DotNetAuth.OAuth1a.OAuth1aProcess.GetAuthorizationUri(new DotNetAuth.OAuth1a.Providers.FlickrOAuth1a(), FlickrAppCredentials, callback, oauth10aStateManager);
            authorizationUri.Wait();
            return Redirect(authorizationUri.Result.AbsoluteUri);
        }
        public ActionResult FlickrResponseProcess()
        {
            var response = DotNetAuth.OAuth1a.OAuth1aProcess.ProcessUserResponse(new DotNetAuth.OAuth1a.Providers.FlickrOAuth1a(), FlickrAppCredentials, Request.Url, oauth10aStateManager);
            response.Wait();
            Session["flickraccesstoken"] = response.Result.AllParameters[DotNetAuth.OAuth1a.Framework.Names.AccessTokenResponse.oauth_token];
            return View("AccessTokens");
        }

        public RedirectResult Vimeo()
        {
            var callback = Url.Action("VimeoResponseProcess", "Auth", routeValues: null, protocol: Request.Url.Scheme);
            var authorizationUri = DotNetAuth.OAuth1a.OAuth1aProcess.GetAuthorizationUri(new DotNetAuth.OAuth1a.Providers.VimeoOAuth1a(), VimeoAppCredentials, callback, oauth10aStateManager);
            authorizationUri.Wait();
            return Redirect(authorizationUri.Result.AbsoluteUri);
        }
        public ActionResult VimeoResponseProcess()
        {
            var response = DotNetAuth.OAuth1a.OAuth1aProcess.ProcessUserResponse(new DotNetAuth.OAuth1a.Providers.VimeoOAuth1a(), VimeoAppCredentials, Request.Url, oauth10aStateManager);
            response.Wait();
            Session["vimeoaccesstoken"] = response.Result.AllParameters[DotNetAuth.OAuth1a.Framework.Names.AccessTokenResponse.oauth_token];
            return View("AccessTokens");
        }
    }
}
